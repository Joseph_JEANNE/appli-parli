                                                Appli-Parli
                                            Projet réalisé par
                                    Spencer GALLONI et Joseph JEANNE
                /!\ Cette application nécessite 2 téléphones distincts pour fonctionner. /!\


1. Interface Utilisateur et Fonctionnement

L'interface se présente sous la forme suivant:
- 2 boutons en haut de l'écran
- 1 talkie-walkie au centre en tant que bouton poussoir
- 1 bouton en bas de l'écran

Les 2 boutons en haut de l'écran sont les boutons de Advertising ainsi que de Discovery.
Le premier appareil doit "Start Advertising", puis le second "Start Discovering".
Un toast apparaitra lors de la connexion entre les deux appareils. Vos téléphones sont maintenant connectés.
En maintenant enfoncé le talkie-walkie au centre et en parlant à votre micro, vous pouvez donc communiquer avec l'autre.
N'hésitez pas à enfoncer le bouton, attendre un peu, parler, attendre un peu, puis relacher.
Nous avons testé avec un succès la discussion sur une ligne droite en extérieur avec une distance entre les deux appareils
de plus de 150 mètres !
Lors de la fin de votre discussion, vous êtes libre d'appuyer sur le bouton "Disconnect" en bas de votre écran.
Si vous laissez l'application en arrière-plan, ou même vérouiller votre téléphone, vous recevrez toujours les communications.

2. Fonctionnement de l'Application

L'application utilise le Nearby Connections API pour communiquer entre vos appareils, cette méthode permet la communication par Bluetooth mais aussi par WiFi entre les deux appareils.
Nous utilisons ici des Payload de File, la communication ne s'effectue donc pas en direct mais en différée.
Pour ce qui est de l'enregistrement nous utilisons MediaPlayer ainsi que MediaRecorder.
L'enregistrement est lancé, l'enregistrement s'arrête et le fichier se finalise, nous envoyons le fichier audio et une fois celui-ci entièrement reçu nous lançons sa lecture.

3. Améliorations Possibles

L'envoi de l'audio en direct en passant par des Payload de Bytes, en découpant donc l'audio en chunck.
Amélioration de l'interface utilisateur.
Permettre la connection de plus que 2 appareils.
Permettre de choisir un canal de communication spécifique.
Permettre de communiquer par morse en se servant, pourquoi pas, de la Notification Led.
Régler les bugs existants.

4. Les Bugs Existants et Connues


Bug du non maintient du bouton Talkie-walkie: Si l'on clique à plusieurs reprises, sans maintenir, sur le talkie-walkie l'application cesse de fonctionner.
Raison: L'enregistement n'a pas le temps de se prepare puis start, donc l'arrêt ne fonctionne pas et fait cesser le fonctionnement de l'appli.

Bug de la non localisation des fichiers: Il est impossible sur certains téléphones de lire le fichier audio reçu. Ne provoque pas l'arrêt de l'application cependant.
Raison: Inconnue, soupçon que le bug soit lié à l'utilisation d'une carte SD.

Bug du non discover: Il est impossible sur certains téléphones de Discover mais seulement de Advertise. Ne provoque pas l'arrêt de l'application.
Raison: Inconnue.
