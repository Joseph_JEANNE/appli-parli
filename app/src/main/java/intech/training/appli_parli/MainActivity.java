package intech.training.appli_parli;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    Button buttonStartAdvertise;
    Button buttonStartDiscovery;
    Button buttonSendFile;
    Button buttonDisconnect;
    String endpointidV;
    MediaRecorder microphone = new MediaRecorder();
    Boolean verif = false;
    Boolean onAd = false;
    Boolean onDis = false;

    final Strategy STRATEGY = Strategy.P2P_STAR;
    final String UserName = getDeviceName();
    final String SERVICE_ID = "intech.training.appli_parli";
    Activity currentActivity = this;
    final ConnectionLifecycleCallback connectionLifecycleCallback = new ConnectionLifecycleCallback() {
        @Override
        public void onConnectionInitiated(@NonNull String endpointId, @NonNull ConnectionInfo connectionInfo) {
            Nearby.getConnectionsClient(currentActivity).acceptConnection(endpointId, mPayloadCallback);
            Log.i("TAG", "connection info: "+connectionInfo.getEndpointName());
            Toast.makeText(getApplicationContext(), "Connected to "+connectionInfo.getEndpointName(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionResult(@NonNull String endPointId, @NonNull ConnectionResolution connectionResolution) {
            switch (connectionResolution.getStatus().getStatusCode()) {
                case ConnectionsStatusCodes.STATUS_OK:
                    // We're connected! Can now start sending and receiving data.
                    String strendPointId = endPointId;
                    endpointidV = strendPointId;
                    sendPayLoad(strendPointId);
                    break;
                case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                    // The connection was rejected by one or both sides.
                    break;
                case ConnectionsStatusCodes.STATUS_ERROR:
                    // The connection broke before it was able to be accepted.
                    break;
                default:
                    // Unknown status code
            }
        }

        @Override
        public void onDisconnected(@NonNull String s) {

        }
    };
    final EndpointDiscoveryCallback endpointDiscoveryCallback = new EndpointDiscoveryCallback() {
        @Override
        public void onEndpointFound(final String endpointId, final DiscoveredEndpointInfo info) {
            // An endpoint was found. We request a connection to it.
            Nearby.getConnectionsClient(currentActivity)
                    .requestConnection(UserName, endpointId, connectionLifecycleCallback)
                    .addOnSuccessListener(
                            new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void unused) {
                                    // We successfully requested a connection. Now both sides
                                    // must accept before the connection is established.
                                    Log.i("TAG", "End point info: "+endpointId+" Info: "+info);
                                    Toast.makeText(getApplicationContext(), "Connected to "+info.getEndpointName(), Toast.LENGTH_LONG).show();
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    // Nearby Connections failed to request the connection.
                                }
                            });
        }

        @Override
        public void onEndpointLost(String endpointId) {
            // A previously discovered endpoint has gone away.
        }
    };
    private final PayloadCallback mPayloadCallback = new PayloadCallback() {
        @Override
        public void onPayloadReceived(@NonNull String s, @NonNull Payload payload) {
            final   byte[] receivedBytes = payload.asBytes();
            verif = true;
        }
        @Override
        public void onPayloadTransferUpdate(@NonNull String s,
                                            @NonNull PayloadTransferUpdate payloadTransferUpdate) {
            if (payloadTransferUpdate.getStatus() == PayloadTransferUpdate.Status.SUCCESS && verif) {
                File latestFile = getLastModified(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Nearby");
                Log.i("TAG", "Le fichier : "+latestFile);
                Log.i("TAG", "Le dossier: "+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
                //latestFile.renameTo(new File(latestFile, latestFile.getName()+".mp3"));
                MediaPlayer mp = new MediaPlayer();
                try {
                    mp.setDataSource(latestFile.getAbsolutePath());
                    mp.prepare();
                    mp.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                verif = false;
                Log.i("TAG", "Reading file.");
            }
        }
    };

    private void sendPayLoad(final String endPointId) {

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStartAdvertise = findViewById(R.id.buttonStartAdvertise);
        buttonStartDiscovery = findViewById(R.id.buttonStartDiscovery);
        buttonDisconnect = findViewById(R.id.disconnect);
        buttonSendFile = findViewById(R.id.sendFile);

        buttonDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Nearby.getConnectionsClient(currentActivity).stopAllEndpoints();
                onDis = false;
                onAd = false;
                buttonStartAdvertise.setText("Start Advertising");
                buttonStartDiscovery.setText("Start Discovering");
                stopDiscovery();
                stopAdvertising();
                Toast.makeText(getApplicationContext(), "Disconnected", Toast.LENGTH_LONG).show();
            }
        });
        buttonSendFile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    startRecordingFile();
                } else if(event.getAction() == MotionEvent.ACTION_UP) {
                    stopAndSendRecording();
                }
                return false;
            }
        });
        buttonStartAdvertise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!onAd) {
                    buttonStartAdvertise.setText("Stop Advertising");
                    startAdvertising();
                    onAd = true;
                } else if(onAd) {
                    buttonStartAdvertise.setText("Start Advertising");
                    stopAdvertising();
                    onAd = false;
                }
            }
        });

        buttonStartDiscovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!onDis) {
                    buttonStartDiscovery.setText("Stop Discovering");
                    startDiscovery();
                    onDis = true;
                } else if(onDis) {
                    buttonStartDiscovery.setText("Start Discovering");
                    stopDiscovery();
                    onDis = false;
                }
            }
        });

        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                {Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.MANAGE_EXTERNAL_STORAGE}, 8034);
    }

    public static File getLastModified(String directoryFilePath)
    {
        File directory = new File(directoryFilePath);
        File[] files = directory.listFiles();
        long lastModifiedTime = Long.MIN_VALUE;
        File chosenFile = null;

        if (files != null)
        {
            for (File file : files)
            {
                if (file.lastModified() > lastModifiedTime)
                {
                    chosenFile = file;
                    lastModifiedTime = file.lastModified();
                }
            }
        }

        return chosenFile;
    }

    private void stopAndSendRecording() {
        Thread sendingVoiceNote = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    microphone.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                File fileToSend = new File(Objects.requireNonNull(getExternalCacheDir()).getAbsolutePath(), "audiorecordtest.3gp");
                try {
                    Payload filePayload = Payload.fromFile(fileToSend);
                    Nearby.getConnectionsClient(currentActivity).sendPayload(endpointidV, filePayload);
                } catch (FileNotFoundException e) {
                    Log.e("MyApp", "File not found", e);
                }
                Log.i("TAG", "Stop and sending.");
            }
        });
        sendingVoiceNote.start();
    }

    private void startRecordingFile() {
        Thread makingVoiceNote = new Thread(new Runnable() {
            @Override
            public void run() {
                String fileName = getExternalCacheDir().getAbsolutePath();
                fileName += "/audiorecordtest.3gp";
                microphone.setOutputFile(fileName);
                microphone.setAudioSource(MediaRecorder.AudioSource.MIC);
                microphone.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                microphone.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                try {
                    microphone.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                microphone.start();
                Log.i("TAG", "Start recording...");
            }
        });
        makingVoiceNote.start();
    }

    private void startAdvertising() {

        Thread advertisingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                AdvertisingOptions advertisingOptions =
                        new AdvertisingOptions.Builder().setStrategy(STRATEGY).build();
                Nearby.getConnectionsClient(currentActivity)
                        .startAdvertising(
                                UserName, SERVICE_ID, connectionLifecycleCallback, advertisingOptions)
                        .addOnSuccessListener(
                                new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        // We're advertising!
                                        Log.i("TAG", "Succ Advertising");
                                        Toast.makeText(getApplicationContext(), "Advertising....", Toast.LENGTH_LONG).show();
                                    }
                                })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // We were unable to start advertising.
                                        Log.e("TAG", "Suck Advertising");
                                        Toast.makeText(getApplicationContext(), "Advertising unsuccessful !", Toast.LENGTH_LONG).show();
                                        onAd = false;
                                        buttonStartAdvertise.setText("Start Advertising");
                                        e.printStackTrace();
                                    }
                                });
            }
        });
        advertisingThread.start();
    }

    private  void stopAdvertising () {
        try {
            Nearby.getConnectionsClient(currentActivity).stopAdvertising();
            Log.i("TAG", "Stop Advertising succ");
        } catch (Exception e) {
            Log.i("TAG", "Stop Advertising suck");
            e.printStackTrace();
        }
    }


    private void startDiscovery() {

        Thread discoveryThread = new Thread(new Runnable() {
            @Override
            public void run() {
                DiscoveryOptions discoveryOptions =
                        new DiscoveryOptions.Builder().setStrategy(STRATEGY).build();
                Nearby.getConnectionsClient(currentActivity)
                        .startDiscovery(SERVICE_ID, endpointDiscoveryCallback, discoveryOptions)
                        .addOnSuccessListener(
                                new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        // We're discovering!
                                        Toast.makeText(getApplicationContext(), "Discovering...", Toast.LENGTH_LONG).show();
                                        Log.i("TAG", "Succ Discovery: "+endpointDiscoveryCallback);
                                    }
                                })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // We're unable to start discovering.
                                        Log.e("TAG", "Suck Discovery");
                                        Toast.makeText(getApplicationContext(), "Discovering unsuccessful !", Toast.LENGTH_LONG).show();
                                        onDis = false;
                                        buttonStartDiscovery.setText("Start Discovering");
                                        e.printStackTrace();
                                    }
                                });
            }
        });
        discoveryThread.start();
    }

    private void stopDiscovery () {
        try {
            Nearby.getConnectionsClient(currentActivity).stopDiscovery();
            Log.i("TAG", "Stop Discovery succ");
        } catch (Exception e) {
            Log.i("TAG", "Stop Discovery suck");
            e.printStackTrace();
        }
    }

    /** Returns the consumer friendly device name */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    public void checkPermission(String permission, int requestCode)
    {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                MainActivity.this,
                permission)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat
                    .requestPermissions(
                            MainActivity.this,
                            new String[] { permission },
                            requestCode);
        }
        else {
            Toast
                    .makeText(MainActivity.this,
                            "Permission already granted",
                            Toast.LENGTH_SHORT)
                    .show();
        }
    }

}